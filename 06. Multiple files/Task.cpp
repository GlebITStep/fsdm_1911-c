#include "Task.h"

Task::Task(const char* name, bool done)
{
	setName(name);
	this->done = done;
}

char* Task::getName()
{
	return name;
}

bool Task::getDone()
{
	return done;
}

void Task::setName(const char* name)
{
	if (strlen(name) > 0)
	{
		strcpy_s(this->name, name);
	}
}

void Task::setDone(bool done)
{
	this->done = done;
}