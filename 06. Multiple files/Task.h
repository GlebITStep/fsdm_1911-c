#pragma once
#include <iostream>
using namespace std;

class Task
{
private:
	char name[100];
	bool done;

public:
	Task() { }
	Task(const char* name, bool done = false);
	char* getName();
	bool getDone();
	void setName(const char* name);
	void setDone(bool done);
};