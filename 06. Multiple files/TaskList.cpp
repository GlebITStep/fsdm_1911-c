#include "TaskList.h";

TaskList::TaskList()
{
	tasks = nullptr;
	taskCount = 0;
}

TaskList::~TaskList()
{
	delete[] tasks;
}

void TaskList::addTask(Task task)
{
	Task* temp = new Task[taskCount + 1];
	for (int i = 0; i < taskCount; i++)
	{
		temp[i] = tasks[i];
	}
	delete[] tasks;
	tasks = temp;
	tasks[taskCount] = task;
	taskCount++;
}

void TaskList::printTasks()
{
	for (int i = 0; i < taskCount; i++)
	{
		cout << tasks[i].getName() << endl;
	}
}