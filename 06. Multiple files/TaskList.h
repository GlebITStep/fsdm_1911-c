#pragma once
#include "Task.h";

class TaskList
{
private:
	Task* tasks;
	int taskCount;

public:
	TaskList();
	~TaskList();
	void addTask(Task task);
	void printTasks();
};