#include <iostream>
#include <string>
using namespace std;

template <class T>
struct ListNode
{
	T data;
	ListNode* next = nullptr;
};

template <class T>
class List
{
private:
	ListNode<T>* head = nullptr;
	ListNode<T>* tail = nullptr;
	int size = 0;

public:
	//void pushBack(T data)
	//{
	//	if (head == nullptr)
	//	{
	//		head = new ListNode<T>;
	//		head->data = data;
	//	}
	//	else
	//	{
	//		ListNode<T>* node = head;
	//		while (node->next != nullptr)
	//		{
	//			node = node->next;
	//		}
	//		node->next = new ListNode<T>;
	//		node->next->data = data;
	//	}
	//	size++;
	//}

	void pushBack(T data)
	{
		if (head == nullptr)
		{
			head = new ListNode<T>;
			tail = head;
			head->data = data;
		}
		else
		{
			tail->next = new ListNode<T>;
			tail = tail->next;
			tail->data = data;
		}
		size++;
	}

	void insert(T data, int position = 0)
	{
		ListNode<T>* newNode = new ListNode<T>;
		newNode->data = data;

		if (position == 0)
		{
			newNode->next = head;
			head = newNode;
		}
		else
		{
			ListNode<T>* prev = head;
			for (int i = 0; i < position - 1; i++)
			{
				prev = prev->next;
			}
			ListNode<T>* temp = prev->next;
			prev->next = newNode;
			newNode->next = temp;
		}
	}

	void insert(T data, ListNode<T>* prev)
	{
		ListNode<T>* newNode = new ListNode<T>;
		newNode->data = data;
		ListNode<T>* temp = prev->next;
		prev->next = newNode;
		newNode->next = temp;
	}

	//void remove(int position);
	//int search(T data);
	
	ListNode<T>* at(int position)
	{
		ListNode<T>* node = head;
		for (int i = 0; i < position; i++)
		{
			node = node->next;
		}
		return node;
	}

	int getSize() 
	{
		return size;
	}

	void print()
	{
		if (head != nullptr)
		{
			ListNode<T>* node = head;
			do
			{
				cout << node->data << '\t';
				node = node->next;
			} while (node != nullptr);
		}
	}
};



void main()
{
	List<int> list;			//
	list.pushBack(11);		//11
	list.pushBack(22);		//11, 22
	list.pushBack(33);		//11, 22, 33
	list.insert(99, 0);		//99, 11, 22, 33
	list.insert(88, 2);		//99, 11, 88, 22, 33

	list.insert(77, list.at(2));

	list.print();
}
