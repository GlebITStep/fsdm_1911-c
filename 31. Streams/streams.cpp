#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Person
{
public:
	string name;
	string surname;
	int age;
	double salary;

	Person(string name, string surname, int age, double salary)
	{
		this->name = name;
		this->surname = surname;
		this->age = age;
		this->salary = salary;
	}

	Person() {}
};

ostream& operator<<(ostream& out, const Person& p)
{
	out << p.name << endl;
	out << p.surname << endl;
	out << p.age << endl;
	out << p.salary << endl;
	return out;
}

istream& operator>>(istream& in, Person& p)
{
	getline(in, p.name);
	getline(in, p.surname);
	in >> p.age;
	in >> p.salary;
	in.ignore();
	return in;
}

void main() 
{
	vector<Person> people;
	ifstream fin("contacts.txt");
	Person person;
	while (fin >> person)
	{
		people.push_back(person);
	}

	for (auto& person : people)
	{
		cout << person;
	}




	//vector<Person> people;

	//Person person1("Sabik", "Mehdiev", 22, 8888.95);
	//Person person2("Gleb", "Skripnikov", 25, 9999.95);
	//Person person3("Nigar", "Khasaeva", 18, 8889.95);
	//
	//people.push_back(person1);
	//people.push_back(person2);
	//people.push_back(person3);

	//ofstream fout("contacts.txt");
	//for (int i = 0; i < people.size(); i++)
	//{
	//	fout << people[i];
	//}
	




	//Person person;
	//ifstream fin("text.txt");
	//fin >> person;
	//cout << person;




	//Person person("Sabik", "Mehdiev", 22, 8888.95);
	//ofstream fout("text.txt");
	//fout << person;





	//Person person;
	//ifstream fin("text.txt");
	//getline(fin, person.name);
	//getline(fin, person.surname);
	//fin >> person.age;
	//fin >> person.salary;

	//cout << person;




	//Person person("Gleb", "Skripnikov", 25, 9999.95);
	//ofstream fout("text.txt");
	//fout << person.name << endl;
	//fout << person.surname << endl;
	//fout << person.age << endl;
	//fout << person.salary << endl;
	////fout.close();





	//ifstream fin("text.txt");
	//string text;
	//getline(fin, text);
	//cout << text;




	//ifstream fin("text.txt");
	//int num1, num2;
	//fin >> num1;
	//fin >> num2;
	//cout << num1 << " - " << num2 << endl;





	//ofstream fout("text.txt", ios::app);	
	//string text;
	//getline(cin, text);
	//fout << text << endl;




	//cout.fill('_');
	//for (int i = 0; i < 1000; i++)
	//{
	//	cout << setw(10) << left << i;
	//}




	//double pi = 3.14;
	//cout << fixed << setprecision(5) << pi << endl;




	//bool test = true;
	//cout << boolalpha << test << endl;
	//cout.unsetf(ios::boolalpha);
	//cout << test << endl;

	//cout.setf(ios::boolalpha);
	//cout.setf(ios::showpos);
	//cout << test << endl;
	//cout << 123 << endl;

	//cout << (test ? "true" : "false") << endl;





	//int age;
	//while (true)
	//{
	//	cout << "Enter age: ";
	//	cin >> age; //asd

	//	if (cin.fail())
	//	{
	//		cout << "Error! invalid input!\n";
	//		cin.clear();
	//		cin.ignore(32767, '\n');
	//		continue;
	//	}

	//	if (!(age >= 18 && age <= 100))
	//	{
	//		cout << "Error! Your age not in range 18 - 100!\n";
	//		continue;
	//	}

	//	break;
	//};
	//cout << "Your age is " << age << endl;






	//int num;
	//for (int i = 0; i < 10; i++)
	//{
	//	cin >> num;
	//	cout << num << endl;
	//}






	//int num;
	//char* name = new char[10];

	////cin: ""
	//cin >> num;

	////cin: "\n";
	//cin.ignore();

	////cin: ""
	//cin.getline(name, 10);

	//cout << name << " - " << num << endl;






	////cin cout

	//int num, num2, num3;

	////cin: "" 
	//cin >> num; //"123asd"
	//cout << num << endl;

	////cin: "asd"
	//cin >> num2;
	//cout << num2 << endl;

	////cin: "asd"
	//cin >> num3;
	//cout << num3 << endl;
}
