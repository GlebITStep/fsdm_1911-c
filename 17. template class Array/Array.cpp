#include <iostream>
#include <string>
using namespace std;

template <class T>
class Array
{
private:
	T* arr = nullptr;
	int size = 0;

	static int compare(const void* first, const void* second)
	{
		T x = *((const T*)first);
		T y = *((const T*)second);

		if (x > y) return 1;
		else if (x < y) return -1;
		else return 0;
	}
public:

	//  void insert(int data, int position)
	//	void remove(int data)

	~Array()
	{
		if (arr != nullptr)
		{
			delete[] arr;
		}
	}

	void clear()
	{
		if (arr != nullptr)
		{
			delete[] arr;
		}
		size = 0;
	}

	void pushBack(T data)
	{
		T* temp = new T[size + 1];
		for (int i = 0; i < size; i++)
		{
			temp[i] = arr[i];
		}
		delete[] arr;
		arr = temp;
		arr[size] = data;
		size++;
	}

	void removeAt(int position)
	{
		if (position >= 0 && position < size)
		{
			T* temp = new T[size - 1];
			for (int i = 0; i < position; i++)
			{
				temp[i] = arr[i];
			}
			for (int i = position + 1; i < size; i++)
			{
				temp[i] = arr[i];
			}
			delete[] arr;
			arr = temp;
			size--;
		}
	}

	T& operator[](int index) const
	{
		return arr[index];
	}

	int getSize() const
	{
		return size;
	}

	int indexOf(T data) //-1 or index
	{
		for (int i = 0; i < size; i++)
		{
			if (arr[i] == data)
			{
				return i;
			}
		}
		return -1;
	}

	void sort()
	{
		qsort(arr, size, sizeof(T), Array::compare);
	}
};

template <class T>
ostream& operator<<(ostream& out, const Array<T>& arr)
{
	for (int i = 0; i < arr.getSize(); i++)
	{
		out << arr[i] << "; ";
	}
	return out;
}

int compare(const void* first, const void* second) //-1 0 1
{
	int x = *((const int*)first);
	int y = *((const int*)second);

	if (x > y) return 1;
	else if (x < y) return -1;
	else return 0;
}

class Person
{
	string name;
	int age;

public:

	Person(string name = "", int age = 0)
	{
		this->name = name;
		this->age = age;
	}

	bool operator>(const Person& person)
	{
		return this->name > person.name;
	}

	bool operator<(const Person& person)
	{
		return this->name < person.name;
	}

	friend ostream& operator<<(ostream& out, const Person& person)
	{
		out << person.name << " " << person.age;
		return out;
	}
};

void main()
{
	//int arr[3] = { 99, 11, 55 };
	//qsort(arr, 3, sizeof(int), compare);

	//for (int i = 0; i < 3; i++)
	//{
	//	cout << arr[i] << endl;
	//}

	Array<Person> arr;

	arr.pushBack(Person("Gleb", 25));
	arr.pushBack(Person("Firdovsy", 33));
	arr.pushBack(Person("Test", 18));

	cout << arr << endl;
	arr.sort();
	cout << arr << endl;



	//Array<string> arr;

	//arr.pushBack("One");
	//arr.pushBack("Two");
	//arr.pushBack("Three");

	//cout << arr << endl;
	//arr.sort();
	//cout << arr << endl;




	//print(5);
	//print(4.6);
	//print('a');
}