#include <iostream>
#include <string>
using namespace std;

template <class T>
class Stack
{
private:
	const int capacity;
	T* arr = new T[capacity];
	int count = 0;

public:
	Stack(int capacity = 10) : capacity(capacity)
	{
		
	}	

	void push(T data)
	{
		if (count < capacity)
		{
			arr[count] = data;
			count++;
		}
		else
		{
			cout << "ERROR! Stack overflow!" << endl;
		}
	}

	T pop() 
	{
		if (count != 0)
		{
			count--;
			return arr[count];
		}
		return 0;
	}

	T peek() 
	{
		return arr[count - 1];
	}
};

void main()
{
	Stack<int> stack(100);
	stack.push(11);		//11
	stack.push(22);		//11 22
	stack.push(33);		//11 22 33

	cout << stack.peek() << endl;

	stack.push(44);		//11 22 33 44
	stack.push(55);		//11 22 33 44 55

	cout << stack.peek() << endl;

	stack.pop();		// 11 22 33 44
	
	cout << stack.peek() << endl;

	stack.pop();		//11 22 33
	stack.pop();		//11 22

	cout << stack.peek() << endl;

	stack.push(99);		//11 22 99

	cout << stack.peek() << endl;
}
