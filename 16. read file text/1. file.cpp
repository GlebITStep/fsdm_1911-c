#include "Console.h"
#include <string>
using namespace std;

int fsize(FILE* file)
{
	int size = 0;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	fseek(file, 0, SEEK_SET);
	return size;
}

void main()
{
	FILE* file;
	fopen_s(&file, "C:/Users/gspos/Desktop/text.txt", "r"); //r w a rb wb ab
	int size = fsize(file);
	char* buffer = new char[size + 1];
	fread_s(buffer, size, 1, size, file);
	buffer[size] = '\0';
	fclose(file);

	string str = buffer;
	cout << str;
}