#include <iostream>
using namespace std;

class Car
{
	char model[100];
	short year;
	double maxSpeed;
	int power;

public:
	// Конструктор по умолчанию
	// Default constructor
	Car()
	{
		strcpy_s(model, "Unknown");
		year = 1900;
		maxSpeed = 0;
		power = 0;
	}

	// Перегруженный конструктор
	// Overloaded constructor
	Car(const char* model, short year, double maxSpeed, int power) : Car() // Делегирование конструктора
	{
		setModel(model);
		setYear(year);
		setMaxSpeed(maxSpeed);
		setPower(power);

		//strcpy_s(this->model, model);
		//this->year = year;
		//this->maxSpeed = maxSpeed;
		//this->power = power;
	}

	char* getModel()
	{
		return model;
	}

	short getYear()
	{
		return year;
	}

	double getMaxSpeed()
	{
		return maxSpeed;
	}

	int getPower()
	{
		return power;
	}

	void setModel(const char* model)
	{
		if (strlen(model) > 0)
		{
			strcpy_s(this->model, model);
		} 
	}

	void setYear(short year)
	{
		if (year >= 1900 && year <= 2019)
		{
			this->year = year;
		}
	}

	void setMaxSpeed(double maxSpeed)
	{
		if (maxSpeed >= 0 && maxSpeed <= 1000)
		{
			this->maxSpeed = maxSpeed;
		}
	}

	void setPower(int power) 
	{
		if (power >= 0 && power <= 2000)
		{
			this->power = power;
		}
	}

	void showInfo()
	{
		cout << "Model: " << model << endl;
		cout << "Max speed: " << maxSpeed << endl;
		cout << "Power: " << power << endl;
		cout << "Year: " << year << endl << endl;
	}
};

void main() 
{
	//Car car1;
	//car1.setModel("Porsche 911");
	//car1.setMaxSpeed(350);
	//car1.setPower(330);
	//car1.setYear(2016);
	Car car1("Porsche 911", 2016, 350, 330);
	car1.showInfo();

	Car car2;
	car2.setModel("Lada Priora");
	car2.setMaxSpeed(170);
	car2.setPower(110);
	car2.setYear(2010);
	car2.showInfo();

	Car car3;
	car3.showInfo();
}