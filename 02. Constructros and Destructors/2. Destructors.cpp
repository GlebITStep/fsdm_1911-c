#include <iostream>
using namespace std;

class Car
{
	char* model;
	short year = 1900;
	double maxSpeed = 0;
	int power = 0;

public:
	//Car(const char* model, short year, double maxSpeed, int power) 
	//	: year(year), maxSpeed(maxSpeed), power(power) // Initializer list
	//{
	//	strcpy_s(this->model, model);
	//}

	//Car(const char* model, short year, double maxSpeed, int power)
	//{
	//	strcpy_s(this->model, model);
	//	this->year = year;
	//	this->maxSpeed = maxSpeed;
	//	this->power = power;
	//}

	Car(const char* model = "Unknown", short year = 1900, double maxSpeed = 0, int power = 0)
	{
		setModel(model);
		setYear(year);
		setMaxSpeed(maxSpeed);
		setPower(power);
		//cout << "Car created!" << endl;
	}

	~Car()
	{
		delete[] model;
		//cout << "Car deleted!" << endl;
	}

	char* getModel()
	{
		return model;
	}

	short getYear()
	{
		return year;
	}

	double getMaxSpeed()
	{
		return maxSpeed;
	}

	int getPower()
	{
		return power;
	}

	void setModel(const char* model) //"Lada Priora" - 12
	{
		int size = strlen(model);
		if (size > 0)
		{
			delete[] this->model;
			this->model = new char[size + 1];
			strcpy_s(this->model, size + 1, model);
		} 
	}

	void setYear(short year)
	{
		if (year >= 1900 && year <= 2019)
		{
			this->year = year;
		}
	}

	void setMaxSpeed(double maxSpeed)
	{
		if (maxSpeed >= 0 && maxSpeed <= 1000)
		{
			this->maxSpeed = maxSpeed;
		}
	}

	void setPower(int power) 
	{
		if (power >= 0 && power <= 2000)
		{
			this->power = power;
		}
	}

	void showInfo()
	{
		cout << "Model: " << model << endl;
		cout << "Max speed: " << maxSpeed << endl;
		cout << "Power: " << power << endl;
		cout << "Year: " << year << endl << endl;
	}
};

void main() 
{
	//cout << "Begin" << endl;
	//if (true)
	//{
	//	Car car;
	//	cout << "Test" << endl;
	//}
	//cout << "End" << endl;





	//Car car1;
	//car1.setModel("Porsche 911");
	//car1.setMaxSpeed(350);
	//car1.setPower(330);
	//car1.setYear(2016);

	Car car1("Porsche 911", 2016, 350, 330);
	while (true)
	{
		car1.setModel("Lada Priora");
	}

	//Car car2;
	//car2.setModel("Lada Priora");
	//car2.setMaxSpeed(170);
	//car2.setPower(110);
	//car2.setYear(2010);
	//car2.showInfo();
	//Car car2("Lada Priora", 2010);
	//car2.showInfo();

	//Car car3;
	//car3.showInfo();
}