#include<iostream>
#include<string>
#include<array>
#include<vector>
#include<forward_list>
#include<list>
#include<stack>
#include<queue>
#include<deque>
using namespace std;

class Person
{
public:
	string name;
	int age;

	Person(string name, int age)
	{
		this->name = name;
		this->age = age;
	}
};

ostream& operator<<(ostream& out, const Person& person)
{
	out << person.name << "\t" << person.age << endl;
	return out;
}

bool operator<(const Person& first, const Person& second)
{
	return first.age < second.age;
}

bool operator>(const Person& first, const Person& second)
{
	return first.age > second.age;
}

void main()
{
	priority_queue<Person, vector<Person>, greater<Person>> container;

	container.push(Person("Gleb", 25));	
	container.push(Person("Sabik", 20));	
	container.push(Person("Nigar", 18));	

	cout << container.top() << endl;

}