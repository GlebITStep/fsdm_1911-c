#include<iostream>
#include<string>
#include<vector>
using namespace std;

// Abstract class
class Animal
{
public:
	string name;

	Animal(string name)
	{
		this->name = name;
		cout << "Constructor Animal!" << endl;
	}

	virtual void info()
	{
		cout << "Animal name is " << this->name << endl;
	}

	//// Virtual method
	//virtual void say()
	//{
	//	cout << name << " says ???" << endl;
	//}

	//// Pure virtual method
	virtual void say() = 0
	{
		cout << name << " says ???" << endl;
	}

	//virtual ~Animal()
	//{
	//	cout << "Destructor Animal!" << endl;
	//}

	//// Pure virtual destructor
	virtual ~Animal() = 0 {};
};

class Cat : public Animal
{
public:
	Cat(string name) : Animal(name)
	{
		cout << "Constructor Cat!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says meow" << endl;
	}

	virtual void info() override final 
	{
		cout << "Cat name is " << name << endl;
	}

	~Cat()
	{
		cout << "Destructor Cat!" << endl;
	}
};

class Lion final : public Cat
{
public:
	Lion(string name) : Cat(name)
	{
		cout << "Constructor Lion!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says arrr" << endl;
	}

	//virtual void info() override
	//{
	//	cout << "Lion name is " << name << endl;
	//}

	~Lion()
	{
		cout << "Destructor Lion!" << endl;
	}
};

class Dog : public Animal
{
public:
	Dog(string name) : Animal(name)
	{
		cout << "Constructor Dog!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says woof" << endl;
	}

	~Dog()
	{
		cout << "Destructor Dog!" << endl;
	}
};

class Fish : public Animal
{
public:
	Fish(string name) : Animal(name) 
	{
		cout << "Constructor Fish!" << endl;
	}

	virtual void info() override
	{
		cout << "Fish name is " << name << endl;
	}

	virtual void say() override
	{
		cout << name << " is silent" << endl;
	}

	~Fish()
	{
		cout << "Destructor Fish!" << endl;
	}
};

class Person
{
public:
	string passportId;
	string name;
	int age;

	Person(string passportId = "000000", string name = "Empty", int age = 0)
	{
		this->passportId = passportId;
		this->name = name;
		this->age = age;
	}
};

void main()
{
	if (true)
	{
		Animal* animal = new Cat("Barsik");
		delete animal;
	}




	//Cat cat("Barsik");
	//Dog dog("Igor");
	//Fish fish("Nemo");
	//Lion lion("Ruha");
	//Person person("AZ123456", "Firdovsi", 30);

	//Animal* animal = &cat;
	//animal->info();
	//animal->say();
	//cout << endl;

	//animal = &dog;
	//animal->info();
	//animal->say();
	//cout << endl;

	//animal = &fish;
	//animal->info();
	//animal->say();
	//cout << endl;

	//animal = &lion;
	//animal->info();
	//animal->say();
	//cout << endl;




	//Animal& animal = cat;
	//animal.say();
	//animal = dog;
	//animal.say();




	//func(cat);
	//func(dog);




	//vector<Animal*> pets;
	//pets.push_back(&cat);
	//pets.push_back(&dog);
	//for (auto& item : pets)
	//{
	//	item->say();
	//}



	//Animal& animal = cat;
	//animal.info();

	//animal = dog;
	//animal.info();



	//Animal* animal;

	//animal = &cat;
	//animal->info();

	//animal = &dog;
	//animal->info();




	//Animal& animalRef = dog;
	//Animal* animalPtr = &cat;



	//Cat cat("Barsik");
	//cat.info();
	//cat.say();

	//cout << endl;

	//Dog dog("Igor");
	//dog.info();
	//dog.say();
}
