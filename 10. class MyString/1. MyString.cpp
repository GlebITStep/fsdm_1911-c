class MyString
{
private:
	char* str;
	int size;

public:
	MyString(); //default constructor
	MyString(const char* text); //overloaded constructor
	MyString(const MyString& myString); //copy constructor
	~MyString(); //destructor

	void concat(MyString str); // strcat // "Hello".Concat("World") => "HelloWorld"
	void replace(MyString text); // strcpy // "Hello".Concat("World") = > "World"
	bool isEqual(MyString str); // strcmp
	void input(); // input string

	int getSize() const
	{
		return size;
	}

	const char* getStr() const
	{
		return str;
	}
};