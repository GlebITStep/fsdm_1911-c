#include<iostream>
#include<string>
#include<vector>
#include<array>
using namespace std;

class HashTable
{
private:
	array<vector<int>, 5> table;

	int hashFunction(int data)
	{
		return data % 5;
	}

public:
	void add(int data)
	{
		int hash = hashFunction(data);
		table[hash].push_back(data);
	}

	bool search(int data)
	{
		int hash = hashFunction(data);
		for (int i = 0; i < table[hash].size(); i++)
		{
			if (table[hash][i] == data)
				return true;
		}
		return false;
	}
};

void main()
{	
	HashTable hashTable;

	hashTable.add(81);
	hashTable.add(18);
	hashTable.add(77);

	if (hashTable.search(18))
	{
		cout << "YES!";
	}
}