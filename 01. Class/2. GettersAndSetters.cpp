#include <iostream>
using namespace std;

enum Mode { Heat, Cool, Dry, Fan };

class AirConditioning
{
	int temperature;
	bool isPowered;
	Mode mode;

public:
	// Методы-аксессоры

	// Геттер (метод-инспектор)
	int getTemperature()
	{
		return temperature;
	}

	// Сеттер (метод-модификатор)
	void setTemperature(int temperature)
	{
		if (temperature >= 17 && temperature <= 30)
		{
			this->temperature = temperature;
		}
	}

	Mode getMode()
	{
		return mode;
	}

	void setMode(Mode mode)
	{
		if (mode >= Heat && mode <= Fan)
		{
			this->mode = mode;
		}
	}

	void SwitchPower()
	{
		isPowered = !isPowered;
	}

	void Status()
	{
		if (isPowered)
		{
			cout << "AC is on" << endl;
			cout << "Temp: " << temperature << " C" << endl;
			cout << "Mode: ";
			switch (mode)
			{
			case Heat:
				cout << "Heat" << endl;
				break;
			case Cool:
				cout << "Cool" << endl;
				break;
			case Dry:
				cout << "Dry" << endl;
				break;
			case Fan:
				cout << "Fan" << endl;
				break;
			default:
				break;
			}

		}
		else
		{
			cout << "AC is off" << endl;
		}
	}
};

//Инкапсуляция
//Наследование
//Полиморфизм

void main()
{
	AirConditioning ac;
	//ac.isPowered = true;
	ac.SwitchPower();
	//ac.temperature = 20;
	ac.setTemperature(20);
	//ac.mode = Cool;
	ac.setMode(Cool);
	ac.Status();

	cout << endl;

	//ac.isPowered = false;
	ac.SwitchPower();
	ac.Status();

	cout << endl;

	//ac.isPowered = true;
	ac.SwitchPower();
    //ac.temperature = 17;
	ac.setTemperature(17);
	ac.Status();
}