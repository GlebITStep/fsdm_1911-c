#include <iostream>
using namespace std;

class Person
{
public:
	char name[100];

	void SayHello()
	{
		cout << "Hello from " << name << endl;
	}
};

void main()
{
	Person gleb;
	strcpy_s(gleb.name, "Gleb");
	gleb.SayHello();

	Person fuad;
	strcpy_s(fuad.name, "Fuad");
	fuad.SayHello();
}