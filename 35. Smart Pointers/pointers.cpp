#include <iostream>
#include <string>
#include <vector>
using namespace std;

//class Person
//{
//public:
//	string name;
//	int age;
//	weak_ptr<Person> spouse;
//
//	Person(string name, int age)
//	{
//		this->name = name;
//		this->age = age;
//	}
//};
//
//void main()
//{
//	auto a = make_shared<Person>("A", 20);
//	auto b = make_shared<Person>("B", 20);
//
//	a->spouse = b;
//	b->spouse = a;
//
//	//b.reset();
//
//	cout << a->name << " ";
//	if (auto spouse = a->spouse.lock())
//	{
//		cout << spouse->name << endl;
//	}
//	else
//	{
//		cout << "No spouse!";
//	}
//}




class Car
{
public:
	string carNumber;
	string model;

	Car(string carNumber, string model)
	{
		this->carNumber = carNumber;
		this->model = model;
	}
};

class Driver
{
public:
	string driverCardId;
	string fullname;
	shared_ptr<Car> car;

	Driver(string driverCardId, string fullname)
	{
		this->driverCardId = driverCardId;
		this->fullname = fullname;
	}
};

void main()
{
	Driver gleb("123456", "Gleb Skripnikov");
	gleb.car = make_shared<Car>("999999", "Tesla Model S");

	cout << gleb.fullname << endl;
	cout << gleb.car->model << endl;
	
	Driver sabik("654321", "Sabik Mehdiev");
	sabik.car = gleb.car;

	cout << sabik.fullname << endl;
	cout << sabik.car->model << endl;
}