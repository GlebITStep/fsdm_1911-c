#include<iostream>
using namespace std;

//LIFO - Last in first out  (Stack)
//FIFO - First in first out (Queue)

template <class T>
class Queue
{
private:
	const int capacity;
	T* arr = new T[capacity];
	int count = 0;

public:
	Queue(int capacity = 10) : capacity(capacity)
	{

	}

	~Queue()
	{
		delete[] arr;
	}

	void push(T data)
	{
		if (count < capacity)
		{
			arr[count] = data;
			count++;
		}
		else
		{
			cout << "ERROR! Queue overflow!" << endl;
		}
	}

	T shift()
	{
		T temp = arr[0];
		for (int i = 0; i < count - 1; i++)
		{
			swap(arr[i], arr[i + 1]);
		}
		count--;
		return temp;
	}

	T peek()
	{
		return arr[0];
	}
};

void main()
{
	Queue<int> queue;

	queue.push(11);
	queue.push(22);
	queue.push(33);
	queue.push(44);
	queue.push(55);

	queue.shift();
	queue.shift();
	queue.shift();

	cout << queue.peek() << endl;
}

