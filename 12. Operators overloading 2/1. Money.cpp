#include <iostream>
#include <ctime>

using namespace std;

class Money
{
private:
	unsigned int dollars = 0;
	unsigned int cents = 0;

public:
	Money(unsigned int dollars = 0, unsigned int cents = 0)
	{
		this->dollars = dollars;
		this->cents = cents;
	}

	Money(double money)
	{
		this->dollars = (int)money;
		this->cents = ((int)(money * 100) % 100);
	}

	unsigned int getDollars() const { return dollars; };
	
	unsigned int getCents() const { return cents; };
	
	void setDollars(unsigned int dollars) { this->dollars = dollars; };
	
	void setCents(unsigned int cents)
	{ 
		dollars += cents / 100; 
		this->cents = cents % 100; 
	};

	Money operator+(const Money &m2)
	{
		Money result;
		result.dollars = this->dollars + m2.dollars;
		result.setCents(this->cents + m2.cents);
		return result;
	}

	Money operator+(const double& m2)
	{
		Money result;
		result.dollars = this->dollars + (int)m2;
		result.setCents(this->cents + ((int)(m2 * 100) % 100));
		return result;
	}

	bool operator==(const Money& money) const
	{
		return this->dollars == money.dollars && this->cents == money.cents;
	}

	bool equals(const Money& money) const
	{
		return this->dollars == money.dollars && this->cents == money.cents;
	}

	//++x
	Money& operator++()
	{
		this->cents++;
		return *this;
	}

	//x++
	Money operator++(int)
	{
		Money temp(*this);
		this->cents++;
		return temp;
	}

	operator int() const
	{ 
		return getDollars(); 
	}

	operator double() const
	{
		return getDollars() + getCents() / 100.0;
	}

	operator float() const
	{
		return getDollars() + getCents() / 100.0;
	}
};

ostream& operator<<(ostream& out, const Money& money)
{
	out << money.getDollars() << "." << money.getCents() << "$";
	return out;
}

istream& operator>>(istream& in, Money& money)
{
	int dollars = 0;
	int cents = 0;
	cout << "Dollars: ";
	in >> dollars;
	cout << "Cents: ";
	in >> cents;
	money.setDollars(dollars);
	money.setCents(cents);
	return in;
}

Money operator+(const double &m1, const Money &m2)
{
	Money result;
	result.setDollars(m2.getDollars() + (int)m1);
	result.setCents(m2.getCents() + ((int)(m1 * 100) % 100));
	return result;
}

void main()
{
	//Money money1;
	//cout << "Enter your money: \n";
	//cin >> money1;
	//cout << money1 << endl;
	

	Money money1(120, 55);
	Money money2(125, 55);
	//cout << money1 << endl;

	//cout << money1++ << endl;
	//cout << ++money1 << endl;

	//cout << money1 << endl;


	int num = money1;
	cout << num << endl;

	float num2 = money1;
	cout << num2 << endl;

	if (money1.equals(money2))
	{
		 
	}
}

