#include <iostream>
using namespace std;

class Test
{
public: 
	int* ptr;

	Test(int number)
	{
		ptr = new int { number };
		cout << "Constructor\n";
	}

	Test(const Test& test) = delete;

	Test(Test&& test)
	{
		this->ptr = test.ptr;
		test.ptr = nullptr;
		cout << "Move constructor\n";
	}

	Test& operator=(const Test& test) = delete;

	Test& operator=(Test&& test)
	{
		delete ptr;
		this->ptr = test.ptr;
		test.ptr = nullptr;
		cout << "Move assignement\n";
		return *this;
	}

	~Test()
	{
		if (ptr != nullptr)
		{
			delete ptr;
			cout << "Destructor\n";
		}
	}
};

void func(Test t)
{
	cout << "func " << *t.ptr << endl;
}

Test func2()
{
	Test ft(11);
	cout << "func " << *ft.ptr << endl;
	return ft;
}

void main()
{
	//Test mt = func2();

	Test t1(11);
	cout << *t1.ptr << endl;
	Test t2(22);
	cout << *t2.ptr << endl;

	//t1 = Test(33);
	//cout << *t1.ptr << endl;

	t1 = move(t2);
	cout << *t1.ptr << endl;
	//cout << *t2.ptr << endl;
}