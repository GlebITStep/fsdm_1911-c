#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "My first game");

	sf::RectangleShape player(sf::Vector2f(200, 200));
	
	sf::Texture igor;
	igor.loadFromFile("igor.jpg");

	player.setPosition(500, 500);
	player.setFillColor(sf::Color::Green);
	player.setTexture(&igor);
	player.setOrigin(100, 100);

	float playerScale = 1;
	float playerAngle = 0;

	while (window.isOpen())
	{
		//EVENTS
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::MouseMoved)
			{
				//std::cout << event.mouseMove.x << " - " << event.mouseMove.y << std::endl;
				player.setPosition(event.mouseMove.x, event.mouseMove.y);
			}

			if (event.type == sf::Event::MouseButtonPressed) //sf::Event::MouseButtonReleased
			{
				if (event.mouseButton.button == sf::Mouse::Right)
					player.scale(playerScale += 0.001, playerScale += 0.001);
				if (event.mouseButton.button == sf::Mouse::Left)
					player.scale(playerScale -= 0.001, playerScale -= 0.001);
			}

			if (event.type == sf::Event::MouseWheelScrolled)
			{
				//std::cout << event.mouseWheelScroll.delta << std::endl;
				player.setRotation(playerAngle += event.mouseWheelScroll.delta * 5);
			}
		}


		//LOGIC
		//???


		//DRAWING
		window.clear(sf::Color::Black);
		window.draw(player);
		window.display();
	}

	return 0;
}