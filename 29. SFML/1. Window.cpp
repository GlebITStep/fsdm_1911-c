#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(500, 500), "Hello!");
	
	sf::RectangleShape rect;
	rect.setSize(sf::Vector2f(50, 50));
	rect.setPosition(sf::Vector2f(0, 0));
	rect.setFillColor(sf::Color::Red);
	rect.setOutlineColor(sf::Color::Blue);
	rect.setOutlineThickness(10);

	sf::RectangleShape rect2;
	rect2.setSize(sf::Vector2f(50, 50));
	rect2.setPosition(sf::Vector2f(400, 50));
	rect2.setFillColor(sf::Color::Blue);
	rect2.setOutlineColor(sf::Color::Black);
	rect2.setOutlineThickness(10);
	rect2.setOrigin(sf::Vector2f(25, 25));

	float x = 0, y = 0;
	float angle = 0;
	float scaleX = 1, scaleY = 1;

	while (window.isOpen())
	{
		//EVENTS
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		//LOGIC
		if (x < 450 && y < 450)
		{
			x += 0.1;
			y += 0.1;
			rect.setPosition(sf::Vector2f(x, y));
		}
	
		rect2.setRotation(angle += 0.1);
		
		scaleX -= 0.0000001;
		scaleY += 0.0000001;
		rect2.scale(scaleX, scaleY);


		//DRAWING
		//1 - Clear window
		window.clear(sf::Color(69, 217, 89));
		
		//2 - Drawing
		window.draw(rect);
		window.draw(rect2);

		//3 - Display
		window.display();
	}

	return 0;
}