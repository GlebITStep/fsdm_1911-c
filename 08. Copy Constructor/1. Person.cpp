#include <iostream>
using namespace std;

class Person
{
public:
	int age;
	char* name = nullptr;

	explicit Person(int size)
	{
		name = new char[size];
	}

	Person(const char* name, int age): age(age)
	{
		int length = strlen(name) + 1;
		this->name = new char[length];
		strcpy_s(this->name, length, name);
	}

	~Person()
	{
		delete[] name;
	}

	//Copy-constructor
	Person(const Person& p)
	{
		memcpy(this, &p, sizeof(Person));
		
		int length = strlen(p.name) + 1;
		this->name = new char[length];
		strcpy_s(this->name, length, p.name);
	}

	void print() 
	{
		cout << name << " - " << age << endl;
	}
};

//void print(Person p)
//{
//	cout << p.name << " - " << p.age << endl;
//}

void main()
{
	////Person p1("Gleb", 25);
	////print(p1);
	////print(p1);

	////Person p1("Gleb", 25);
	//Person p1 = {"Gleb", 25}; //Person p1("Gleb", 25);
	//Person p2(p1); //Person p2 = p1;
	//
	////Person p2;
	////memcpy(&p2, &p1, sizeof(Person));


	//p1.print();
	//p2.print();

	//cout << endl;

	////p1.name[0] = 'Q';

	//p1.print();
	//p2.print();
}