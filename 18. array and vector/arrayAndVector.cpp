#include <iostream>
#include <string>
#include <array>
#include <vector>

using namespace std;

void main()
{
	vector<vector<int>> matrix = 
	{ 
		{1, 2, 3}, 
		{4, 5, 6}, 
		{7, 8, 9} 
	};

	cout << matrix[0][5] << endl;

	//for (auto& row : matrix)
	//{
	//	for (auto& item : row)
	//	{
	//		cout << item << '\t';
	//	}
	//	cout << endl;
	//}

	//for (int i = 0; i < matrix.size(); i++)
	//{
	//	for (int j = 0; j < matrix[i].size(); j++)
	//	{
	//		cout << matrix[i][j] << '\t';
	//	}
	//	cout << endl;
	//}





	//vector<string> strings = { "One", "Two", "Three" };
	//for (auto& item : strings)
	//{
	//	cout << item << endl;
	//}




	//vector<int> arr = { 11, 22, 33, 44, 55 };
	////arr.push_back(99);
	////arr.pop_back();

	////cout << *(arr.begin() + 2) << endl;
	////cout << *(arr.end() - 3) << endl;

	////arr[3] = 99;
	////arr.insert(arr.begin() + 3, 99); //arr.insert(3, 99);
	////arr.insert(arr.end() - 2, 88);

	////arr.erase(arr.begin() + 2);
	////arr.erase(arr.end() - 3);
	////arr.erase(arr.begin() + 1, arr.end() - 1);

	//for (auto& item : arr)
	//{
	//	cout << item << endl;
	//}



	//vector<int> arr = { 11, 22, 33, 44, 55 };
	//arr.push_back(66);
	//arr.push_back(77);
	//arr.push_back(88);
	//cout << arr.size() << endl;
	////cout << arr[3] << endl;
	////arr = { 1, 2, 3 };
	////arr.assign(9, 55);

	//for (auto& item : arr)
	//{
	//	cout << item << endl;
	//}



	//array<int, 5> arr = { 11, 22, 33, 44, 55 };

	////foreach
	//for (auto& item : arr)
	//{
	//	cout << item << endl;
	//}




	//array<int, 5> arr = { 11, 22, 33, 44, 55 };
	//arr.fill(99);
	//arr.empty();
	//arr.size();
	//
	//for (int i = 0; i < arr.size(); i++)
	//{
	//	cout << arr[i] << endl;
	//}





	//const int size = 5;
	//int arr[size] = { 11, 22, 33, 44, 55 };

	//for (int i = 0; i < size; i++)
	//{
	//	cout << arr[i] << endl;
	//}
}
