#include<iostream>
#include<string>
#include<vector>
using namespace std;

// Abstract class
class Animal
{
public:
	string name;

	Animal(string name)
	{
		this->name = name;
		cout << "Constructor Animal!" << endl;
	}

	virtual void info()
	{
		cout << "Animal name is " << this->name << endl;
	}

	//// Virtual method
	//virtual void say()
	//{
	//	cout << name << " says ???" << endl;
	//}

	//// Pure virtual method
	virtual void say() = 0
	{
		cout << name << " says ???" << endl;
	}

	//virtual ~Animal()
	//{
	//	cout << "Destructor Animal!" << endl;
	//}

	//// Pure virtual destructor
	virtual ~Animal() = 0 {};
};

class Cat : public Animal
{
public:
	Cat(string name) : Animal(name)
	{
		cout << "Constructor Cat!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says meow" << endl;
	}

	virtual void info() override final
	{
		cout << "Cat name is " << name << endl;
	}

	void meow()
	{
		cout << "MEOW!!!";
	}

	~Cat()
	{
		cout << "Destructor Cat!" << endl;
	}
};

class Lion final : public Cat
{
public:
	Lion(string name) : Cat(name)
	{
		cout << "Constructor Lion!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says arrr" << endl;
	}

	//virtual void info() override
	//{
	//	cout << "Lion name is " << name << endl;
	//}

	~Lion()
	{
		cout << "Destructor Lion!" << endl;
	}
};

class Dog : public Animal
{
public:
	Dog(string name) : Animal(name)
	{
		cout << "Constructor Dog!" << endl;
	}

	virtual void say() override
	{
		cout << name << " says woof" << endl;
	}

	~Dog()
	{
		cout << "Destructor Dog!" << endl;
	}
};

class Fish : public Animal
{
public:
	Fish(string name) : Animal(name)
	{
		cout << "Constructor Fish!" << endl;
	}

	virtual void info() override
	{
		cout << "Fish name is " << name << endl;
	}

	virtual void say() override
	{
		cout << name << " is silent" << endl;
	}

	~Fish()
	{
		cout << "Destructor Fish!" << endl;
	}
};

class Person
{
public:
	string name;
	string passportId;
	int age;

	Person(string passportId = "000000", string name = "Empty", int age = 0)
	{
		this->passportId = passportId;
		this->name = name;
		this->age = age;
	}

	void work()
	{
		cout << "WORK!";
	}
};

void test(const Person* p)
{
	//p->work();
	const_cast<Person*>(p)->work();
}

void main()
{


	//Animal* animal = nullptr;
	//int choise;
	//cout << "Choose animal (1 - Cat, 2 - Dog, 3 - Fish): ";
	//cin >> choise;

	//switch (choise)
	//{
	//case 1:
	//	animal = new Cat("Barsik");
	//	break;
	//case 2:
	//	animal = new Dog("Muhtar");
	//	break;
	//case 3:
	//	animal = new Fish("Ruha");
	//	break;
	//}
	
	////RTTI - Run time type identification
	////cout << typeid(*animal).name();
	//if (typeid(*animal) == typeid(Cat))
	//{
	//	//Cat* cat = dynamic_cast<Cat*>(animal);
	//	//cat->meow();

	//	Person* cat = dynamic_cast<Person*>(animal);
	//	//Person* cat = (Person*)animal;
	//	if (cat != nullptr)
	//	{
	//		cout << cat->name;
	//	}
	//	else
	//	{
	//		cout << "Error!!!";
	//	}
	//	//cat->meow();
	//}
	//else
	//{
	//	cout << "NO!";
	//}





	//static_cast
	//dynamic_cast
	//cosnt_cast
	//reinterpret_cast

	//double pi = 3.14;
	//cout << (int)pi << endl;
	//cout << static_cast<int>(pi) << endl;
}
