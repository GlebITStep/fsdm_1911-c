#include<iostream>
#include<string>
using namespace std;

class Device
{
public:
	bool on;

	Device()
	{
		cout << "ctor Device\n";
	}

	~Device()
	{
		cout << "~ Device\n";
	}
};

class Printer : public Device
{
public:
	Printer()
	{
		cout << "ctor Printer\n";
	}

	~Printer()
	{
		cout << "~ Printer\n";
	}

	void print()
	{
		cout << "print\n";
	}

	void info()
	{
		cout << "I can print!\n";
	}
};

class Scaner : public Device
{
public:
	Scaner()
	{
		cout << "ctor Scaner\n";
	}

	~Scaner()
	{
		cout << "~ Scaner\n";
	}

	void scan()
	{
		cout << "scan\n";
	}

	void info()
	{
		cout << "I can scan!\n";
	}
};

class Copier : public Printer, public Scaner
{
public:
	Copier()
	{
		cout << "ctor Copier\n";
	}

	~Copier()
	{
		cout << "~ Copier\n";
	}

	void info()
	{
		Printer::info();
		Scaner::info();
		cout << Printer::on << endl;
		cout << Scaner::on << endl;
	}
};

void main()
{
	Copier copier;
	copier.print();
	copier.scan();
	copier.info();
}