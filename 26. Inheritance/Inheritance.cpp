#include<iostream>
#include<string>
using namespace std;

// Base class (Super class)
class Person
{
private: 
	string passportId;

protected:
	string name;
	int age;

public:
	string info;

	void printInfo()
	{
		cout << "Name: " << name << endl;
		cout << "Age: " << age << endl;
	}

	Person(string name, int age)
	{
		this->name = name;
		this->age = age;
	}
};

// Derived class
class Student : public Person
{
protected:
	double average;

public: 
	Student(string name, int age, double average) : Person(name, age)
	{
		this->average = average;
	}

	void printInfo()
	{
		Person::printInfo();
		cout << "Average: " << average << endl;
	}
};

void main() 
{
	Person p("Gleb", 25);
	p.printInfo();

	cout << endl;

	Student s("Gleb", 25, 12);
	s.printInfo();
}