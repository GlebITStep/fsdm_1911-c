#include <iostream>
using namespace std;

class Task
{
private:
	char name[100];
	bool done;

public:
	Task()
	{
		
	}

	Task(const char* name, bool done = false)
	{
		setName(name);
		this->done = done;
	}

	char* getName()
	{
		return name;
	}

	bool getDone()
	{
		return done;
	}

	void setName(const char* name)
	{
		if (strlen(name) > 0)
		{
			strcpy_s(this->name, name);	
		}
	}

	void setDone(bool done)
	{
		this->done = done;
	}
};

class TaskList
{
private:
	Task* tasks;
	int taskCount;

public:
	TaskList()
	{
		tasks = nullptr;
		taskCount = 0;
	}

	~TaskList()
	{
		delete[] tasks;
	}

	void addTask(Task task)
	{
		Task* temp = new Task[taskCount + 1];
		for (int i = 0; i < taskCount; i++)
		{
			temp[i] = tasks[i];
		}
		delete[] tasks;
		tasks = temp;
		tasks[taskCount] = task;
		taskCount++;
	}

	void printTasks()
	{
		for (int i = 0; i < taskCount; i++)
		{
			cout << tasks[i].getName() << endl;
		}
	}
};

void main() 
{
	TaskList list;

	cout << "Enter task count: ";
	int count;
	cin >> count;
	cin.ignore();
	for (int i = 0; i < count; i++)
	{
		char taskName[100];
		cout << "Task #" << i + 1 << ": ";
		cin.getline(taskName, 100);

		//1
		//Task newTask(taskName);
		//list.addTask(newTask);

		//2
		list.addTask(Task(taskName));
	}

	list.printTasks();



	TaskList todayTasks;

	Task task1("Do my homework");
	Task task2("Feed my cat");

	todayTasks.addTask(task1);
	todayTasks.addTask(task2);

	todayTasks.printTasks();

	//cout << endl;
	////---------------------

	//TaskList moviesToWatch;

	//Task task3("Matrix");
	//Task task4("Terminator 2");

	//moviesToWatch.addTask(task3);
	//moviesToWatch.addTask(task4);
	//moviesToWatch.addTask(Task("Matrix 2"));

	//moviesToWatch.printTasks();
}