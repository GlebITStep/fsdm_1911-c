#include <iostream>
using namespace std;

class MyString
{
private:
	char* str;
	int size;

	void copy(const MyString& myString)
	{
		memcpy(this, &myString, sizeof(myString));
		int length = myString.size + 1;
		this->str = new char[length];
		strcpy_s(this->str, length, myString.str);
	}

public:
	MyString() //default constructor
	{ 
		str = nullptr;
		size = 0;
	} 

	MyString(const char* text) //overloaded constructor
	{
		int length = strlen(text) + 1;
		str = new char[length];
		strcpy_s(str, length, text);
		size = length - 1;
	}

	MyString(const MyString& myString) //copy constructor
	{
		copy(myString);
	}

	void operator=(const MyString& myString)
	{
		copy(myString);
	}
	
	~MyString() //destructor
	{
		if (str != nullptr)
		{
			delete[] str;
		}
	}

	int getSize() const
	{
		return size;
	}

	const char* getStr() const
	{
		return str;
	}

	char& operator[](int index)
	{
		return this->str[index];
	}

	//char& operator()()
	//{
	//	return this->str[0];
	//}
};

ostream& operator<<(ostream& out, const MyString& str)
{
	out << str.getStr();
	return out;
}

void test(MyString str)
{
	cout << str << endl;
}

class MyClass
{
	//Default constructor
	//Copy constructor
	//Destructor
	//operator=
};


void main()
{
	//MyString text = "Hello!";
	//cout << text[0] << endl;
	//text[0] = 'O';
	//cout << text << endl;




	//MyClass a;
	//MyClass b = a;
	//MyClass c;
	//c = a;

	//MyString text = "Hello!";
	//MyString text2;
	//text2 = text;
	//cout << text2 << endl;
	//cout << text << endl;
	//test(text);
}