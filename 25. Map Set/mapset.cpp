#include<iostream>
#include<string>
#include<set>
#include<map>
using namespace std;

class Person
{
public:
	string passportId;
	string name;
	int age;

	Person(string passportId = "000000", string name = "Empty", int age = 0)
	{
		this->passportId = passportId;
		this->name = name;
		this->age = age;
	}
};

void main()
{
	// MAP
	map<string, Person> people;

	people.insert({ "AZ123456", Person("AZ123456", "Gleb", 25) });			//1
	people["AZ987654"] = Person("AZ987654", "Sabik", 20);					//2
	people.insert(make_pair("AZ111222", Person("AZ111222", "Nigar", 18)));	//3

	if (people.find("AZ111222") != people.end())
	{
		cout << "FOUND!" << endl;
	}

	for (auto& item : people)
	{
		if (item.second.name == "Nigar")
		{
			cout << item.second.name << endl;
			break;
		}
	}

	cout << people["AZ111222"].name << endl;


	////SET
	//set<int> set;
	//set.insert(55);
	//set.insert(33);
	//set.insert(11);
	//set.insert(44);
	//set.insert(22);

	//// FIND ELEMENT
	//if (set.find(11) != set.end())
	//{
	//	cout << "Found!" << endl;
	//}

	//// PRINT ALL
	//for (auto& item : set)
	//{
	//	cout << item;
	//}
}
