#include <iostream>
using namespace std;

class Person
{
	char name[100];
	int age;
	static int count;

public:
	const int id;

	Person() : id(++Person::count)
	{
		//Person::count++;
	}

	//Person(int id = 0) : id(id)
	//{
	//	Person::count++;
	//}

	//~Person()
	//{
	//	count--;
	//}

	char* getName()
	{
		return name;
	}

	int getAge()
	{
		return age;
	}

	static int getCount()
	{
		return Person::count;
	}

	void setName(const char* name)
	{
		strcpy_s(this->name, name);
	}

	void setAge(int age)
	{
		this->age = age;
	}

	static void setCount(int count)
	{
		if (count > 0)
		{
			Person::count = count;
		}
	}

	void ShowInfo()
	{
		cout << "Count: " << count << endl;
		cout << "Name: " << name << endl;
		cout << "Age: " << age << endl;
	}
};

int Person::count = 0;

void main() 
{
	Person p1;
	cout << p1.id << endl;

	if (true)
	{
		Person p2;
		cout << p2.id << endl;
	}

	Person p3;
	cout << p3.id << endl;


	//Person p1;
	//if (true)
	//{
	//	Person p2;
	//	Person p3;
	//}	
	////Person::setCount(Person::getCount() + 1);
	////cout << Person::getCount() << endl;




	//Person::count = 5;
	//cout << Person::count << endl;




	//Person p1;
	//Person p2;
	//Person p3;
	//Person::count = 1;
	//cout << Person::count << endl;
	//Person::count = 5;
	//cout << Person::count << endl;
}