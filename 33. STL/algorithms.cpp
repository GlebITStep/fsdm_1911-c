#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <list>
#include <set>
using namespace std;

bool moreThan20(int x)
{
	return x > 20;
}

bool isEven(int x)
{
	return x % 2 == 0;
}

bool isInRange(int x)
{
	return x >= 33 && x <= 55;
}

bool my_all_of(vector<int>::iterator begin, vector<int>::iterator end, bool(*predicate)(int))
{
	bool result = true;
	for (auto i = begin; i != end; i++)
	{
		if (!predicate(*i))
		{
			result = false;
			break;
		}
	}
	return result;
}

//class RangeFunctor
//{
//private:
//	int min;
//	int max;
//	
//public:
//	RangeFunctor(int min, int max)
//	{
//		this->min = min;
//		this->max = max;
//	}
//
//	bool operator()(int x)
//	{
//		return x >= min && x <= max;
//	}
//};



class RangeFunctor1
{
private:
	int min;
	int max;

public:
	RangeFunctor1()
	{
		this->min = min;
		this->max = max;
	}

	bool operator()(int x)
	{
		return x >= min && x <= max;
	}
};

void main()
{
	vector<int> container = { 55, 22, 11, 76, 33, 44, 11 };
	int min, max;
	cout << "Min: ";
	cin >> min;
	cout << "Max: ";
	cin >> max;
	//cout << count_if(container.begin(), container.end(), [](int x) { return x % 2 != 0; });
	//cout << count_if(container.begin(), container.end(), isInRange);
	//cout << count_if(container.begin(), container.end(), RangeFunctor(min, max));
	//cout << count_if(container.begin(), container.end(), [](int x) { return x >= 55 && x <= 77; });
	//cout << count_if(container.begin(), container.end(), [](int x) { return x >= 22 && x <= 44; });
	//cout << count_if(container.begin(), container.end(), [&](int x) { return x >= min && x <= max; });
	//cout << count_if(container.begin(), container.end(), [=](int x) { return x >= min && x <= max; });
	cout << count_if(container.begin(), container.end(), [min, max](int x) { return x >= min && x <= max; });
	
	//RangeFunctor funct(55, 77);
	//RangeFunctor funct2(11, 33);
	//cout << funct(66) << endl;
	//cout << funct2(66) << endl;








	//vector<int> container = { 55, 22, 11, 76, 33, 44, 11 };
	///*bool result = all_of(container.begin(), container.end(), moreThan20);*/
	////bool result = any_of(container.begin(), container.end(), moreThan20);
	////bool result = all_of(container.begin(), container.end(), [](int x) { return x > 20; });
	////cout << boolalpha << result << endl;

	////all_of
	////any_of
	////none_of

	////count
	////count_if
	////cout << count(container.begin(), container.end(), 11);
	////cout << count_if(container.begin(), container.end(), isEven);

	////find
	////find_if
	////find_if_not
	////find_end
	///*auto iter = find(container.begin(), container.end(), 11);*/
	///*auto iter = find_if(container.begin(), container.end(), isEven);*/
	////auto iter = find_if_not(container.begin(), container.end(), isEven);
	////if (iter != container.end())
	////	cout << *iter << endl;
	////else
	////	cout << "Not found!" << endl;

	////replace_if
	////replace_if(container.begin(), container.end(), isEven, 0);
	////for (auto& item : container)
	////{
	////	cout << item << '\t';
	////}

	////container.erase(remove_if(container.begin(), container.end(), isEven), container.end());
	////for (auto& item : container)
	////{
	////	cout << item << '\t';
	////}

	////vector<int> container = { 55, 22, 21, 77, 33, 44 };

	////bool test = true;
	////for (int i = 0; i < container.size(); i++)
	////{
	////	if (container[i] <= 20)
	////	{
	////		test = false;
	////		break;
	////	}
	////}

	////cout << boolalpha << test << endl;
}