#include<iostream>
#include<array>
#include<vector>
#include<string>
#include<list>
#include<forward_list>
#include<stack>
#include<queue>
#include<map>
#include<set>
#include<unordered_map>
#include<unordered_set>
#include<tuple>

using namespace std;

tuple<int, int> maxmin(vector<int>& vect)
{
	int max = vect[0];
	int min = vect[0];
	for (auto& item : vect)
	{
		if (item > max)
			max = item;

		if (item < min)
			min = item;
	}
	return { max, min };
}

void main()
{
	//vector<int> vect = { 33, 22, 55, 11, 44 };
	//auto result = maxmin(vect);
	//cout << "Max: " << get<0>(result) << endl;
	//cout << "Min: " << get<1>(result) << endl;





	//// Containers
	//array<int, 10> arr;
	//vector<int> vect;
	//string str;
	//list<int> list;
	//forward_list<int> flist;
	//stack<int> stack;
	//queue<int> queue;
	//deque<int> deque;
	//priority_queue<int> pqueue;
	//set<int> set;
	//map<int, string> map;
	//multiset<int> mset;
	//multimap<int, string> mmap;
	//unordered_set<int> uset;
	//unordered_map<int, string> umap;
	//unordered_multiset<int> umset;
	//unordered_multimap<int, string> ummap;

	////Tuple - Кортеж
	////tuple<int, string, bool> tuple = { 42, "Test", true };
	//auto tuple = make_tuple(42, "Test", true);
	//cout << get<0>(tuple) << endl;
	//cout << get<1>(tuple) << endl;
	//cout << get<2>(tuple) << endl;

	////Pair
	////pair<int, string> pair = { 11, "Hello" };
	//auto pair = make_pair(11, "Hello");
	//cout << pair.first << " " << pair.second << endl;




	// Iterators
	vector<int> container = { 33, 22, 55, 11, 44 };
	//list<int> container = { 33, 22, 55, 11, 44 };
	//set<int> container = { 33, 22, 55, 11, 44 };
	for (auto iter = container.crbegin(); iter != container.crend(); iter++)
	{
		cout << *iter << '\t';
	}
	//for (auto& item : container)
	//{
	//	cout << item << '\t';
	//}

	//vector<int> container = { 33, 22, 55, 11, 44 };
	//auto iter = container.cbegin();
	//iter++;
	////*iter = 99;
	//iter += 3;
	//iter--;
	//cout << *iter << endl;
	//cout << iter[2] << endl;



	// Algorithms





}
