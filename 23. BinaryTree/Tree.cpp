#include<iostream>
#include<string>
using namespace std;

template <class T>
struct TreeNode
{
	TreeNode<T>* parent = nullptr;
	T data;
	TreeNode<T>* left = nullptr;
	TreeNode<T>* right = nullptr;
};

template <class T>
class BinaryTree
{
private:

public:
	TreeNode<T>* root = nullptr;

	TreeNode<T>* search(T data)
	{
		TreeNode<T>* temp = root;

		while (true)
		{
			if (temp == nullptr)
				return nullptr;

			if (temp->data == data)
				return temp;
			else if (data > temp->data)
				temp = temp->right;
			else if (data < temp->data)
				temp = temp->left;
		}
	}

	void insert(T data)
	{
		TreeNode<T>* temp = root;

		if (root == nullptr)
		{
			root = new TreeNode<T>;
			root->data = data;
			return;
		}

		while (true)
		{
			if (temp->data == data)
				return;
			
			if (data > temp->data)
			{
				if (temp->right == nullptr)
				{
					TreeNode<T>* newNode = new TreeNode<T>;
					newNode->data = data;

					temp->right = newNode;
					newNode->parent = temp;
					return;
				}
				temp = temp->right;
			}
			else if (data < temp->data)
			{
				if (temp->left == nullptr)
				{
					TreeNode<T>* newNode = new TreeNode<T>;
					newNode->data = data;

					temp->left = newNode;
					newNode->parent = temp;
					return;
				}
				temp = temp->left;
			}
		}
	}

	void print(TreeNode<T>* node)
	{
		if (node != nullptr)
		{
			print(node->left);
			cout << node->data << endl;
			print(node->right);
		}
	}

	TreeNode<T>* min(TreeNode<T>* node)
	{
		TreeNode<T>* temp = node;
		while (temp->left != nullptr)
		{
			temp = temp->left;
		}
		return temp;
	}

	TreeNode<T>* max(TreeNode<T>* node)
	{
		TreeNode<T>* temp = node;
		while (temp->right != nullptr)
		{
			temp = temp->right;
		}
		return temp;
	}

	void remove(T data)
	{
		TreeNode<T>* deleted = search(data);

		if (deleted == nullptr)
			return;

		if (deleted->left == nullptr && deleted->right == nullptr)
		{
			if (deleted->data > deleted->parent->data)
				deleted->parent->right = nullptr;
			else
				deleted->parent->left = nullptr;
			delete deleted;
		}
		else if (deleted->left == nullptr || deleted->right == nullptr)
		{
			if (deleted->right != nullptr) 
			{
				deleted->data = deleted->right->data;
				delete deleted->right;
				deleted->right = nullptr;
			}
			else
			{
				deleted->data = deleted->left->data;
				delete deleted->left;
				deleted->left = nullptr;
			}
		}
		else
		{
			TreeNode<T>* successor = min(deleted->right);
			deleted->data = successor->data;
			successor->parent->left = nullptr;
			delete successor;
		}
	}
};

void main()
{	
	string text = "Gleb";
	text.find_first_of(".,?!", 0);

	BinaryTree<int> tree;
	tree.insert(11);
	tree.insert(6);
	tree.insert(19);
	tree.insert(4);
	tree.insert(8);
	tree.insert(17);
	tree.insert(43);
	tree.insert(5);
	tree.insert(10);
	tree.insert(31);
	tree.insert(49);

	tree.remove(19);

	tree.print(tree.root);

	//if (tree.search(10) != nullptr)
	//{
	//	cout << "YES" << endl;
	//}
	//else 
	//{
	//	cout << "NO" << endl;
	//}
}
