#pragma once
#include <array>
#include "Resouces.h"

using namespace std;

static class Map
{
private:
	static array<array<int, 30>, 30> map;

public:
	static bool canMove(int x, int y);
	static MapCell getCell(int x, int y);
};

