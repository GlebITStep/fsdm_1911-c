#pragma once
#include <map>
#include "MapCell.h"
#include <SFML/Graphics.hpp>

using namespace std;

enum class MapCell
{
	Grass,
	Wall,
	Tree,
	Water,
	Floor
};

enum class Actors
{
	Player,
	FatGuy
};

enum class Effect
{
	Fog
};

static class Resouces
{
private:
	static sf::Texture sprites;
	static sf::Texture fogTexture;
	static map<MapCell, sf::RectangleShape> mapTextures;
	static map<Actors, sf::RectangleShape> actorTextures;
	static map<Effect, sf::RectangleShape> effectsTextures;

public:
	static void initResources();
	static sf::RectangleShape& getMapTexture(MapCell cell);
	static sf::RectangleShape& getActorTexture(Actors actor);
	static sf::RectangleShape& getEffectTexture(Effect effect);
};

