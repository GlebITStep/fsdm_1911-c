#pragma once
enum class Direction
{
	Up,
	Down,
	Left,
	Right
};

class Actor
{
private:
	int x;
	int y;

public:
	Actor(int x, int y);

	void move(Direction direction);

	int getX();
	int getY();

	void setPosition(int x, int y);

	virtual ~Actor() = 0 {}
};

