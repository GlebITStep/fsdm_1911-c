#include <iostream>
#include <array>
#include <SFML/Graphics.hpp>
#include "Resouces.h"
#include "Map.h"
#include "Player.h"
#include "FatGuy.h"

using namespace std;

class  
{
public:
	int iter = 0;

	void Restart() {
		for (int i = 0; i < SIZE; i++) {
			SAVE[i] = 0;
		}
	}

	void Save() {
		if (is_first) {
			for (int i = 0; i < SIZE; i++) {
				SAVE[i] = iter;
			}
			is_first = false;
		}
		for (int i = SIZE - 1; i > 0; i--) {
			SAVE[i] = SAVE[i - 1];
		}
		SAVE[0] = iter;
		iter = 0;
	}

	int GetTheArithmeticMean() {
		int TAM = 0;
		for (int i = 0; i < SIZE; i++) {
			TAM += SAVE[i];
		}
		return TAM / SIZE;
	}

private:
	int SIZE = 1;
	int SAVE[1];
	bool is_first = true;
} FPS;

int main()
{
	srand(time(0));

	sf::RenderWindow window(sf::VideoMode(960, 960), "My first game");
	Resouces::initResources();

	sf::Clock clock;
	sf::Clock clock2;

	Player player(15, 15);

	vector<FatGuy> fatGuys;
	for (int i = 0; i < 10; i++)
	{
		int x, y;
		do
		{
			x = rand() % 30;
			y = rand() % 30;
		} while (!Map::canMove(x, y));
		fatGuys.push_back(FatGuy(x, y));	
	}

	while (window.isOpen())
	{
		FPS.iter++;
		if (clock2.getElapsedTime().asSeconds() >= 1) {
			FPS.Save();
			window.setTitle(to_string(FPS.GetTheArithmeticMean()));
			clock2.restart();
		}


		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
				case sf::Keyboard::W:
					player.move(Direction::Up);
					break;
				case sf::Keyboard::S:
					player.move(Direction::Down);
					break;
				case sf::Keyboard::A:
					player.move(Direction::Left);
					break;
				case sf::Keyboard::D:
					player.move(Direction::Right);
					break;
				}
			}
		}

		
		if (clock.getElapsedTime().asMilliseconds() > 200)
		{
			for (auto& item : fatGuys)
			{
				item.move((Direction)(rand() % 4));
			}
			clock.restart();
		}
		


		window.clear(sf::Color::Black);
	
		for (int y = 0; y < 30; y++)
		{
			for (int x = 0; x < 30; x++)
			{
				auto shape = Resouces::getMapTexture(Map::getCell(x, y));
				shape.setPosition(x * 32, y * 32);
				window.draw(shape);
			}
		}

		for (auto& item : fatGuys)
		{
			auto enemyShape = Resouces::getActorTexture(Actors::FatGuy);
			enemyShape.setPosition(item.getX() * 32, item.getY() * 32);
			window.draw(enemyShape);
		}

		auto playerShape = Resouces::getActorTexture(Actors::Player);
		playerShape.setPosition(player.getX() * 32, player.getY() * 32);
		window.draw(playerShape);

		auto fogShape = Resouces::getEffectTexture(Effect::Fog);
		fogShape.setPosition(player.getX() * 32 + 15, player.getY() * 32 + 15);
		//window.draw(fogShape);

		window.display();
	}

	return 0;
}