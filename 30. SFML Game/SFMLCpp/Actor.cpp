#include "Actor.h"
#include "Map.h"


Actor::Actor(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Actor::move(Direction direction)
{
	switch (direction)
	{
	case Direction::Up:
		if (Map::canMove(x, y - 1))
			y--;
		break;
	case Direction::Down:
		if (Map::canMove(x, y + 1))
			y++;
		break;
	case Direction::Left:
		if (Map::canMove(x - 1, y))
			x--;
		break;
	case Direction::Right:
		if (Map::canMove(x + 1, y))
			x++;
		break;
	}
}

int Actor::getX()
{
	return x;
}

int Actor::getY()
{
	return y;
}

void Actor::setPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}
