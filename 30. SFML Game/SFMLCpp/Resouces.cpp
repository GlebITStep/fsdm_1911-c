#include "Resouces.h"

sf::Texture Resouces::sprites;
sf::Texture Resouces::fogTexture;
map<MapCell, sf::RectangleShape> Resouces::mapTextures;
map<Actors, sf::RectangleShape> Resouces::actorTextures;
map<Effect, sf::RectangleShape> Resouces::effectsTextures;

void Resouces::initResources()
{
	sprites.loadFromFile("sprites.png");
	fogTexture.loadFromFile("fog.png");

	//MAP
	sf::RectangleShape grass(sf::Vector2f(32, 32));
	grass.setTexture(&sprites);
	grass.setTextureRect(sf::IntRect(0, 480, 32, 32));
	mapTextures[MapCell::Grass] = grass;

	sf::RectangleShape wall(sf::Vector2f(32, 32));
	wall.setTexture(&sprites);
	wall.setTextureRect(sf::IntRect(1664, 544, 32, 32));
	mapTextures[MapCell::Wall] = wall;

	sf::RectangleShape tree(sf::Vector2f(32, 32));
	tree.setTexture(&sprites);
	tree.setTextureRect(sf::IntRect(1440, 416, 32, 32));
	mapTextures[MapCell::Tree] = tree;

	sf::RectangleShape water(sf::Vector2f(32, 32));
	water.setTexture(&sprites);
	water.setTextureRect(sf::IntRect(864, 608, 32, 32));
	mapTextures[MapCell::Water] = water;

	sf::RectangleShape floor(sf::Vector2f(32, 32));
	floor.setTexture(&sprites);
	floor.setTextureRect(sf::IntRect(1888, 416, 32, 32));
	mapTextures[MapCell::Floor] = floor;


	//ACTORS
	sf::RectangleShape player(sf::Vector2f(32, 32));
	player.setTexture(&sprites);
	player.setTextureRect(sf::IntRect(1186, 962, 32, 32));
	actorTextures[Actors::Player] = player;

	sf::RectangleShape fatGuy(sf::Vector2f(32, 32));
	fatGuy.setTexture(&sprites);
	fatGuy.setTextureRect(sf::IntRect(1312, 64, 32, 32));
	actorTextures[Actors::FatGuy] = fatGuy;


	//EFFECTS
	sf::RectangleShape fog(sf::Vector2f(1920, 1920));
	fog.setTexture(&fogTexture);
	fog.setOrigin(960, 960);
	effectsTextures[Effect::Fog] = fog;
}

sf::RectangleShape& Resouces::getMapTexture(MapCell cell)
{
	return mapTextures[cell];
}

sf::RectangleShape& Resouces::getActorTexture(Actors actor)
{
	return actorTextures[actor];
}

sf::RectangleShape& Resouces::getEffectTexture(Effect effect)
{
	return effectsTextures[effect];
}
