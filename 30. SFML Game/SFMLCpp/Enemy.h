#pragma once
#include "Actor.h"

class Enemy : public Actor
{
public:
	Enemy(int x, int y) : Actor(x, y) {}

	virtual ~Enemy() = 0 {}
};

