#include <iostream>
using namespace std;

/*
	//DONE
	ctor
	~
	void pushBack(int data)
	int get(int position);
	void set(int position, int data);
	int getSize()
	void removeAt(int position)
	int indexOf(int data)

	//TODO
	copy ctor
	void insert(int data, int position)
	void remove(int data)
	int lastIndexOf(int data)
	void clear()
	void sort()
*/

class IntArray
{
private:
	int* arr = nullptr;
	int size = 0;

public:

	~IntArray()
	{
		delete[] arr;
	}

	void pushBack(int data)
	{
		int* temp = new int[size + 1];
		memcpy(temp, arr, sizeof(int) * size);
		delete[] arr;
		arr = temp;
		arr[size] = data;
		size++;
	}

	//          3
	//          V
	//11 22 33 44 55 66 77 88 99 = 8
	//11 22 33 __ __ __ __ __ = 7
	void removeAt(int position)
	{
		if (position >= 0 && position < size)
		{
			int* temp = new int[size - 1];
			memcpy(temp, arr, sizeof(int) * position);
			memcpy(temp + position, arr + position + 1, sizeof(int) * (size - position));
			delete[] arr;
			arr = temp;
			size--;
		}
	}

	int get(int position) const
	{
		return arr[position];
	}

	void set(int position, int data)
	{
		arr[position] = data;
	}

	int getSize() const
	{
		return size;
	}

	int indexOf(int data) //-1 or index
	{
		for (int i = 0; i < size; i++)
		{
			if (arr[i] == data)
			{
				return i;
			}
		}
		return -1;
	}
};

void print(const IntArray &arr)
{
	for (int i = 0; i < arr.getSize(); i++)
	{
		cout << arr.get(i) << '\t';
	}
}

//class Student
//{
//	char name[100];
//	IntArray grades;
//
//	void setGrade(int grade)
//	{
//		grades.pushBack(grade);
//	}
//};

void main()
{
	IntArray myArr;

	myArr.pushBack(11);
	myArr.pushBack(22);
	myArr.pushBack(33);
	myArr.pushBack(44);
	myArr.pushBack(55);
	myArr.pushBack(66);
	myArr.pushBack(77);
	myArr.pushBack(99);

	cout << myArr.get(1) << endl;
	myArr.set(2, 99);

	print(myArr);

	cout << endl;

	myArr.removeAt(3);

	print(myArr);

	cout << endl;

	cout << myArr.indexOf(99) << endl;
}