#include<iostream>
#include<string>
#include<vector>
#include<array>
using namespace std;

//class file_not_found : public exception
//{
//	string filename;
//	string message;
//
//public:
//	file_not_found(char const* const _Message, const char* filename) : exception(_Message) 
//	{
//		this->message = _Message;
//		this->filename = filename;
//	}
//
//	char const* what() const override
//	{
//		string errorText = (message + "\nFile name: " + filename);
//		return errorText.c_str();
//	}
//};

class file_not_found : public exception
{
public:
	file_not_found(char const* const _Message) : exception(_Message) {}
};

int arr[5] = { 11,22,0,44,55 };

int getElem(int index)
{
	if (index >= 0 && index < 5)
		return arr[index];
	else
		throw out_of_range("Error! Incorrect index!");
		//throw exception("Error! Incorrect index!");
		//throw "Error! Incorrect index!";
}

double division(double x, double y)
{
	if (y != 0)
		return x / y;
	else
		throw invalid_argument("Error! Second argument can't be zero!");
		//throw exception("Error! Second argument can't be zero!");
		//throw "Error! Division by zero!";
}

string getFileText(string filename)
{
	FILE* file;
	fopen_s(&file, filename.c_str(), "r");
	if (file != nullptr)
	{
		char buffer[10000];
		fgets(buffer, 10000, file);
		fclose(file);
		return buffer;
	}
	throw file_not_found("Error! File not found!");
	//throw exception("Error! File not found!");
}

void main()
{	
	while (true)
	{
		string filename;
		cout << "Enter file name: ";
		getline(cin, filename);
		try
		{
			string result = getFileText(filename);
			cout << "TEXT:" << endl;
			cout << result;
			break;
		}
		catch (const std::exception & ex)
		{
			cout << ex.what() << endl;
		}
	}
	cout << "End!" << endl;



	//cout << "Begin!" << endl;
	//try
	//{
	//	int num1 = getElem(-1); //22
	//	cout << "First number: " << num1 << endl;
	//	int num2 = getElem(2); //0
	//	cout << "Second number: " << num2 << endl;
	//	int result = division(num1, num2); // 22 / 0
	//	cout << "Result: " << result << endl;
	//}
	//catch (exception& ex)
	//{
	//	cout << ex.what() << endl;
	//}
	//cout << "End!" << endl;






	//cout << "Begin!" << endl;
	//try
	//{
	//	int num1 = getElem(-1); //22
	//	cout << "First number: " << num1 << endl;
	//	int num2 = getElem(2); //0
	//	cout << "Second number: " << num2 << endl;
	//	int result = division(num1, num2); // 22 / 0
	//	cout << "Result: " << result << endl;
	//}
	//catch (out_of_range& ex)
	//{
	//	cout << ex.what() << endl;
	//}
	//catch (invalid_argument& ex)
	//{
	//	cout << ex.what() << endl;
	//}
	//catch (...)
	//{
	//	cout << "Unexpected error!" << endl;
	//}
	//cout << "End!" << endl;





	//cout << "Begin!" << endl;
	////vector<int> arr = { 11,22,33,44,55 };
	//array<int, 5> arr = { 11,22,33,44,55 };
	//try
	//{
	//	//int result = arr[-1];
	//	int result = arr.at(-1);
	//	cout << result << endl;
	//}
	//catch (...)
	//{
	//	cout << "Error!" << endl;
	//}
	//cout << "End!";




	//cout << "Begin!" << endl;
	//try
	//{
	//	int num1 = getElem(0); //22
	//	cout << "First number: " << num1 << endl;
	//	int num2 = getElem(2); //0
	//	cout << "Second number: " << num2 << endl;
	//	int result = division(num1, num2); // 22 / 0
	//	cout << "Result: " << result << endl;
	//}
	//catch (const char* ex)
	//{
	//	cout << ex << endl;
	//}
	//catch (int ex)
	//{
	//	cout << "Int error!" << endl;
	//}
	//catch (...)
	//{
	//	cout << "Unexpected error!" << endl;
	//}
	//cout << "End!" << endl;





	//int num1 = getElem(1);
	//cout << "First number: " << num1 << endl;
	//int num2 = getElem(2);
	//cout << "Second number: " << num2 << endl;
	//int result = division(num1, num2);
	//cout << "Result: " << result << endl;




	//cout << division(0, 0) << endl;
	//cout << getElem(10) << endl;
}
